from django.contrib import admin
from projects.models import Project

# Register your models here.


admin.site.register(Project)


# I think it looks better like this instead of using class with pass, I talked
# with a sier and they told me this is also good and i like it more ^_^
